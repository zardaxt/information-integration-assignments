\documentclass{article}
\usepackage{geometry}
\geometry{a4paper, margin=1in}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{minted}
\usemintedstyle{vs}
\usepackage{hyperref}
\usepackage[parfill]{parskip}

\title{Assignment 3 - Information Integration}
\author{Felix Melcher und Nikolai Tschacher}
\date{ }
 
\begin{document}
	
\maketitle

\tableofcontents
\newpage

In the following writeup we will cover the basic steps in order to answer the information integration course assignment 3.

\section{Reading material}

Before we begin the work, it makes sense to read and understand the following ressources:

\begin{enumerate}
	\item The wikipedia article about the Gene Ontology \url{https://en.wikipedia.org/wiki/Gene_ontology}
	\item An tutorial on how to implement DAG's in relational databases: \url{http://www.codeproject.com/Articles/22824/A-Model-to-Represent-Directed-Acyclic-Graphs-DAG-o}
	\item A paper about SQL and Dags's from the Ulf Leser: Querying ontologies in relational database systems \url{https://www2.informatik.hu-berlin.de/Forschung_Lehre/wbi/publications/2005/dils05_ontologies.pdf}
\end{enumerate}


\section{Data Cleaning}

\subsection{Eliminate all entries from your gene table (merged) which do not belong to humans}

The tax\_id for humans is 9606. This means all entries in gene\_info and gene2refseq where tax\_id != 9606 can be discarded. 
We wrote a Python function to create a CSV table from the original CSV files that discards all rows with tax\_id != 9606. This function solves also some of the problems encountered in assignment 2.

\begin{minted}{python}
#!/usr/bin/env python3

import csv

def merge():
    """
    merge_gene2refseq and_geneinfo
    Only keep human rows.
    """
    outfile = open('merged_gene_info.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'symbol', 'chromosome', 'map_location', 'status',
     'protein_accession_version', 'protein_gi', 'start_position_on_the_genomic_accession', 'end_position_on_the_genomic_accession'])

    info_file = open('gene_info_processed.csv', newline='')
    reader_info = csv.reader(info_file, delimiter='\t')
    header = next(reader_info)

    gene_info_data = dict()
    keys = set()

    h = nh = 0
    for row in reader_info:
        if row[0].strip() == '9606' and int(row[0]) == 9606:
            h+=1
            key = (row[0], row[1])
            gene_info_data[key] = [row[2], row[3], row[4]]
            keys.add(key)
        else:
            nh+=1

    print(h, nh)

    refseq_file = open('gene2refseq_processed.csv', newline='')
    reader_refseq = csv.reader(refseq_file, delimiter='\t')
    header_refseq = next(reader_refseq)

    for row in reader_refseq:
        out_tuple = []
        refseq_key = (row[0], row[1])
        out_tuple.extend(refseq_key)

        if refseq_key in gene_info_data:
            out_tuple.extend(gene_info_data[refseq_key])
            out_tuple.extend(row[2:])
            if refseq_key in keys:
                keys.remove(refseq_key)
            writer.writerow(out_tuple)
        # key cannot be found in gene_info but tax_id is still human
        elif row[0].strip() == '9606' and int(row[0]) == 9606:
            out_tuple.extend(3*[''])
            out_tuple.extend(row[2:])
            writer.writerow(out_tuple)

    print('Keys in gene_info but not in gene2refseq: {}'.format(len(keys)))

    for key in keys:
        t = gene_info_data[key]
        writer.writerow([key[0], key[1]] + gene_info_data[key] + (5 * ['']))

    outfile.close()
\end{minted}

Then we clean all no human rows in gene2go: 

\begin{minted}{text}
huii=# delete from gene2go where tax_id != 9606;
DELETE 1582779
\end{minted}

\subsection{The gene table (merged) has duplicates}

Duplicte entries are rows with the same geneid. These may have different status, protein accession, start/end positions etc. We will first eliminate all real duplicates.
These are all rows with the same gene\_id which have all identical attributes for all columns.

We know that there are duplicate rows in the table, because the two queries yield 
different numbers:

\begin{minted}{bash}
huii=# select count(*) from (select distinct * from gene_info_merged) as x;
 count  
--------
 456531
(1 row)

huii=# select count(*) from (select * from gene_info_merged) as x;
 count  
--------
 516946
(1 row)
\end{minted}

To eliminate all duplicate rows, we create a cleaned table with the following query:

\begin{minted}{sql}
CREATE TABLE gene_info_merged_clean
AS SELECT distinct * from gene_info_merged;
\end{minted}

The new table is duplicate free:

\begin{minted}{bash}
huii=# select count(*) from (select * from gene_info_merged_clean) as x;
 count  
--------
 456531
(1 row)
\end{minted}
    
Then we rename some columns for convenience:

\begin{minted}{sql}
ALTER TABLE gene_info_merged_clean
RENAME COLUMN start_position_on_the_genomic_accession TO start;

ALTER TABLE gene_info_merged_clean
RENAME COLUMN end_position_on_the_genomic_accession TO end_pos;
\end{minted}

\subsection{All values which have 1:n relationship to a gene should be moved to a separate table}

One table for the essential data. Start and end position should be saved together.

With the following query we find that the core table of gene\_info should have the following columns: \texttt{tax\_id, gene\_id, symbol, chromosome, map\_location}

\begin{minted}{text}
huii=# select count(*) from
 (select distinct tax_id, gene_id, symbol, chromosome, map_location
 from gene_info_merged_clean) as x;
 count 
-------
 59850
(1 row)

huii=# select count(*) from 
(select distinct gene_id from gene_info_merged_clean) as x;
 count 
-------
 59850
(1 row)
\end{minted}

Therefore our core table looks like this:
\begin{minted}{sql}
CREATE TABLE gene_info_clean (
    tax_id integer,
    gene_id integer,
    symbol varchar(90),
    chromosome varchar(300),
    map_location varchar(300)
);
\end{minted}
    
Then we identified 3 other tables with functional dependencies to the primary key gene\_id:

\begin{minted}{sql}
CREATE TABLE gene_positions (
    gene_id integer,
    start_pos varchar(15),
    end_pos varchar(15)
);

CREATE TABLE gene_protein (
    gene_id integer,
    protein_accession_version varchar(15),
    protein_gi varchar(15)
);

CREATE TABLE gene_status (
    gene_id integer,
    status status_enum
);
\end{minted}
    
Then we will populate these 4 new tables:

\begin{minted}{sql}
INSERT INTO gene_positions(gene_id, start_pos, end_pos) 
SELECT distinct gene_id, start_pos, end_pos from gene_info_merged_clean;

INSERT INTO gene_protein(gene_id, protein_accession_version, protein_gi) 
SELECT distinct gene_id, protein_accession_version, protein_gi from gene_info_merged_clean;

INSERT INTO gene_status(gene_id, status) 
SELECT distinct gene_id, status from gene_info_merged_clean;

INSERT INTO gene_info_clean(tax_id, gene_id, symbol, chromosome, map_location) 
SELECT distinct tax_id, gene_id, symbol, chromosome, map_location from gene_info_merged_clean;
\end{minted}

We now need to make sure that the INNER JOIN of the base table and the 3 new generated tables (\texttt{gene\_positions, gene\_protein, gene\_status}) yields the same number of results as the original table (\texttt{gene\_info\_merged\_clean}) from which we extrated the 3 schematas.

This means that the number of results for the query:

\begin{minted}{sql}
select count(*) from
(select distinct tax_id, gene_id, symbol, chromosome, map_location, status
from gene_info_merged_clean) as x;
\end{minted}

is the same as the number of results of the INNER JOIN of \texttt{gene\_info\_clean and gene\_status}

\begin{minted}{sql}
select count(*) from 
(select distinct x.tax_id, x.gene_id, x.symbol, x.chromosome, x.map_location, y.status
from gene_info_clean x, gene_status y where x.gene_id = y.gene_id) as x;
\end{minted}

The same principle applies for the other 2 tables.


\section{Parse the Gene Ontology}

We write a Python script to parse the Gene Ontology from the \texttt{.obo} file. The script generates a output CSV file with the relevant data: \texttt{id, name, namespace} and n \texttt{is\_a} items.

\begin{minted}{python}
#!/usr/bin/env python3

import csv

def parse_gene_ontology(fname='go-basic.obo'):
    """
    Each new term starts with the delimiter [Term].

    We are interested in the
    - id
    - name
    - namespace
    And n is_a items.
    """
    with open(fname, 'rt') as f:
        terms = []
        term = dict()
        for i, line in enumerate(f):
            line = line.strip()

            if '[Term]' in line:
                if term:
                    if 'is_a' in term:
                        term['is_a'] = '|'.join(term['is_a'])
                    if not ('is_obsolete' in term and term['is_obsolete']):
                        terms.append(term)
                    term = dict()

            if line.startswith('id: '):
                term['id'] = line.lstrip('id: ')

            if line.startswith('name: '):
                term['name'] = line.lstrip('name: ')

            if line.startswith('namespace: '):
                term['namespace'] = line.lstrip('namespace: ')

            if line.startswith('is_obsolete: '):
                if line == 'is_obsolete: true':
                    term['is_obsolete'] = True

            if line.startswith('is_a: '):
                if 'is_a' not in term:
                    term['is_a'] = []
                isa = line.lstrip('is_a: ')
                isa = isa.split(' ! ')[0]
                term['is_a'].append(isa)

        with open('terms.csv', 'wt') as outfile:
            writer = csv.DictWriter(outfile, delimiter='\t',
             fieldnames=('id', 'name', 'namespace', 'is_a'))

            for term in terms:
                writer.writerow(term)

if __name__ == '__main__':
    parse_gene_ontology()
\end{minted}

\section{Connect the data to the Gene2Go data from assignment 2 via ID}

This essentially means that we need to create a schema for a DAG in a relational database. By reading \url{http://www.codeproject.com/Articles/22824/A-Model-to-Represent-Directed-Acyclic-Graphs-DAG-o} we learn that we can model a DAG by creating a table that stores the edges between vertices from a DAG. Therefore we can create a table like

\begin{minted}{sql}
CREATE TABLE go_relationships (
	id SERIAL PRIMARY KEY, -- the edge id
	go_id varchar(10), -- the starting GO node
	go_node varchar(10) -- the end GO node
);
\end{minted}

to store all edges that builds the DAG. Furthermore we need to have a table that stores the Gene Ontology:

\begin{minted}{sql}
CREATE TABLE go_ontology (
    go_id varchar(10),
    go_name varchar(300),
    go_namespace varchar(100)
);
\end{minted}

\subsection{Populate the Gene Ontology}

Now that we parsed the file, it is straightforward to actually read in the two tables generated beforehand. But first we need to strip the \texttt{is\_a} column from the CSV file.

\begin{minted}{python}
#!/usr/bin/env python3

import csv

def strip_column(in_file, out_file, columns=[]):
    """
    Removes column(s) from the csv file.
    """
    with open(in_file, 'rt') as f:
        reader = csv.reader(f, delimiter='\t')

        ff = open(out_file, 'wt')
        writer = csv.writer(ff, delimiter='\t')

        for row in reader:
            for i in columns:
                row.pop(i)
            writer.writerow(row)

        ff.close()

if __name__ == '__main__':
    strip_column('terms.csv', 'go.csv', [3,])
\end{minted}

Now we read in the Gene Ontology data into our postgres database:

\begin{minted}{text}
huii=# \COPY go_ontology(go_id, go_name, go_namespace)
		 FROM 'go.csv' WITH (NULL '', FORMAT CSV, delimiter E'\t', HEADER);
COPY 42832
\end{minted}

Reading in the relationsships requires another preprocessed CSV table with all available relationships between the GO terms. For this we write another short Python script:

\begin{minted}{python}
def create_DAG_csv_file():
    """
    This function creates a CSV file with all
    realationships between the GO terms.

    It outputs a CSV file with two columns:
    - go_id
    - go_node
    which are two vertices that form and edge.
    """
    with open('terms.csv', 'rt') as f:
        reader = csv.reader(f, delimiter='\t')

        ff = open('dag.csv', 'wt')
        writer = csv.writer(ff, delimiter='\t')
        writer.writerow(('go_id', 'go_node'))

        for row in reader:
            realationships = row[3].split('|')
            for rel in realationships:
                writer.writerow((row[0], rel))

        ff.close()
\end{minted}

And then we read in all GO relationsships:

\begin{minted}{text}
huii=# \COPY go_relationships(go_id, go_node)
 FROM 'dag.csv' WITH (NULL '', FORMAT CSV, delimiter E'\t', HEADER);
COPY 73725
\end{minted}

To get the DAG for a certain GO:

\begin{minted}{sql}
SELECT *
     FROM go_ontology
     WHERE go_id IN (
             SELECT go_id
                FROM go_relationships
                WHERE go_node = 'GO:0016021' ) 
\end{minted}

Now we have a problem, because ancestors and successors are not included in the set of all possible edges. For this to work, we would need to store the transitive closure of all GO nodes in the go\_relationship table and then we could select the whole DAG in constant time.

Because looking up data in $O(1)$ is always nice, we try to calculate the transitive closure of all our edges. First lets find out how many edges we have:

\begin{minted}{bash}
nikolai@nikolai:~/.../uebung3_nikolai$ wc -l terms.csv 
42833 terms.csv
\end{minted}

So there are roughly 40.000 distinct nodes in our DAG. The worst case running time to find a transitive closure of a DAG is $O(n^3)$, but it is probably much less because the Gene Ontolgy DAG is probably rather sparse.


\subsection{Recursive SQL query to find ancestors/successors}


\section{Answer to the queries}

\subsection{Queries for Task 1}

\textbf{(1) Number of distinct human genes after deduplication}

\begin{minted}{sql}
huii=# select count(*) from gene_info_clean;
 count 
-------
 59850
(1 row)
\end{minted}

There is no need for the keyword \texttt{distinct} since we already populated the table with distinct rows.


\textbf{(2) Maximal number of different positions (start and end) assigned to a gene}

\begin{minted}{sql}
huii=# select max(cnt) from 
(select gene_id as gi, count(*) as cnt
 from gene_positions
  group by gene_id
    order by count(*) desc) as x;
 max 
-----
  38
(1 row)
\end{minted}


\textbf{(3) How many genes have only one position (start and end)?}

\begin{minted}{sql}
huii=# select count(*) from 
(select gene_id, count(*)
 from gene_positions
  group by gene_id
    having count(*) = 1) as subquery;
 count 
-------
  8369
(1 row)
\end{minted}

\subsection{Queries for Task 2}

\textbf{(4) How many GO terms are there in total and for each ontology?}

The number of GO terms in total can be found with the following query:

\begin{minted}{sql}
huii=# select count(*)
	 from (select distinct go_id from go_ontology) as x ;
 count 
-------
 42832
(1 row)
\end{minted}

The number of GO terms for each of the three Ontologies can be found with the following query:

\begin{minted}{sql}
huii=# select count(*) from 
	(select distinct go_id
	 from go_ontology
	  where go_namespace = 'molecular_function') as x;
 count 
-------
 10238
(1 row)

huii=# select count(*) from
 (select distinct go_id
  from go_ontology
   where go_namespace = 'biological_process') as x;
 count 
-------
 28688
(1 row)

huii=# select count(*) from 
 (select distinct go_id
  from go_ontology
   where go_namespace = 'cellular_component') as x;
 count 
-------
  3906
(1 row)
\end{minted}


\textbf{(5) Compute a frequency histogram over the number of terms assigned to a gene}

\textbf{(6) Write a program (or query) which computes for each term to how many genes it is assigned}


\section{Database Schema}

\begin{figure}[!ht]
  \caption{The database schema}
  \center
    \includegraphics[width=0.93\paperwidth]{schema.pdf}
\end{figure}


\end{document}