\select@language {english}
\contentsline {section}{\numberline {1}Reading material}{2}{section.1}
\contentsline {section}{\numberline {2}Data Cleaning}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Eliminate all entries from your gene table (merged) which do not belong to humans}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}The gene table (merged) has duplicates}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}All values which have 1:n relationship to a gene should be moved to a separate table}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}Parse the Gene Ontology}{5}{section.3}
\contentsline {section}{\numberline {4}Connect the data to the Gene2Go data from assignment 2 via ID}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Populate the Gene Ontology}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Recursive SQL query to find ancestors/successors}{8}{subsection.4.2}
\contentsline {section}{\numberline {5}Answer to the queries}{8}{section.5}
\contentsline {subsection}{\numberline {5.1}Queries for Task 1}{8}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Queries for Task 2}{9}{subsection.5.2}
\contentsline {section}{\numberline {6}Database Schema}{10}{section.6}
