#!/usr/bin/env python3

"""
This algorithm finds the transitive closure
of a set of edges.
"""

import csv

def get_edges():
    edges = list()
    with open('dag.csv', 'rt') as f:
        reader = csv.reader(f, delimiter='\t')
        header = next(reader)
        for row in reader:
            edges.append(row)

    return edges

def get_edges_as_dict():
    edges = dict()
    with open('terms.csv', 'rt') as f:
        reader = csv.reader(f, delimiter='\t')
        header = next(reader)
        for row in reader:
            edges[row[0]] = row[3].split('|')

    return edges


def get_all_vertices(edges):
    s = set()

    for u, v in edges:
        if u not in s:
            s.add(u)
        if v not in s:
            s.add(v)

    return s

def get_set_of_nodes_no_incoming_edge(edges):
    s = get_all_vertices(edges)
    S = set()
    d = {v:u for u,v in edges}

    for v in s:
        if v not in d:
            S.add(v)

    return S

def kahn_topological_sort():
    E = get_edges()
    L = list()
    # S ← Set of all nodes with no incoming edges
    S = get_set_of_nodes_no_incoming_edge(E)
    print(len(S))

    while not S.empty():
        # remove n from S
        node = S.pop()
        # add n to tail of L
        L.append(node)
        # for each node m with an edge e from n to m do
    # 
    #         remove edge e from the graph
    #         if m has no other incoming edges then
    #             insert m into S
    # if graph has edges then
    #     raise Exception('graph has at least one cycle')
    # else
    #     return L


if __name__ == '__main__':
    kahn_topological_sort()
