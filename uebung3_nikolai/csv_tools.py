#!/usr/bin/env python3

import csv

def strip_column(in_file, out_file, columns=[]):
    """
    Removes column(s) from the csv file.
    """
    with open(in_file, 'rt') as f:
        reader = csv.reader(f, delimiter='\t')

        ff = open(out_file, 'wt')
        writer = csv.writer(ff, delimiter='\t')

        for row in reader:
            for i in columns:
                row.pop(i)
            writer.writerow(row)

        ff.close()


def csv_as_list(fname):
    data = []
    with open(fname, 'rt') as f:
        reader = csv.reader(f, delimiter='\t')
        header = next(reader)
        data = [row for row in reader]
    return data

if __name__ == '__main__':
    strip_column('terms.csv', 'go.csv', [3,])
