select count(*) from (select distinct * from gene_info_merged_clean) as x;


# finding functinoal dependencies
select gene_id, start_pos, end_pos, count(*) from gene_info_merged_clean group by gene_id, start_pos, end_pos having count(*) > 1;

select gene_id, protein_gi, count(*) from gene_info_merged_clean group by gene_id, protein_gi having count(*) > 1;

huii=# \d gene_info_merged_clean;
             Table "public.gene_info_merged_clean"
          Column           |          Type          | Modifiers
---------------------------+------------------------+-----------
 tax_id                    | integer                |
 gene_id                   | integer                |
 symbol                    | character varying(90)  |
 chromosome                | character varying(300) |
 map_location              | character varying(300) |
 status                    | status_enum            |
 protein_accession_version | character varying(15)  |
 protein_gi                | character varying(15)  |
 start_pos                 | character varying(15)  |
 end_pos                   | character varying(15)  |



select count(*) from
(   select distinct gimcn.tax_id, gimcn.gene_id, gimcn.symbol, positions.start_pos, positions.end_pos
    from gimcn INNER JOIN positions ON (gimcn.gene_id = positions.gene_id)) as x;

/** the gene core table becomes: **/
select count(*) from (select distinct tax_id, gene_id, symbol, chromosome, map_location  from gene_info_merged_clean) as x;


CREATE TABLE gene_positions (
    gene_id integer,
    start_pos varchar(15),
    end_pos varchar(15)
);

INSERT INTO gene_positions(gene_id, start_pos, end_pos)
SELECT distinct gene_id, start_pos, end_pos from gene_info_merged_clean;


CREATE TABLE gene_protein (
    gene_id integer,
    protein_accession_version varchar(15),
    protein_gi varchar(15)
);

INSERT INTO gene_protein(gene_id, protein_accession_version, protein_gi)
SELECT distinct gene_id, protein_accession_version, protein_gi from gene_info_merged_clean;


CREATE TABLE gene_status (
    gene_id integer,
    status status_enum
);

INSERT INTO gene_status(gene_id, status)
SELECT distinct gene_id, status from gene_info_merged_clean;


CREATE TABLE gene_info_clean (
    tax_id integer,
    gene_id integer,
    symbol varchar(90),
    chromosome varchar(300),
    map_location varchar(300)
);

INSERT INTO gene_info_clean(tax_id, gene_id, symbol, chromosome, map_location)
SELECT distinct tax_id, gene_id, symbol, chromosome, map_location from gene_info_merged_clean;


-- Find entries that have NULL values
select count(*) from gene_protein where protein_gi = '-' and protein_accession_version = '-';

select count(*) from gene_status where status is null;

select count(*) from gene_positions where start_pos = '-' and end_post = '-';


select distinct * from gene_info_clean g, gene_positions p, gene_protein gp, gene_status gs
 where g.gene_id = p.gene_id and g.gene_id = gp.gene_id and g.gene_id = gs.gene_id
 limit 1000;



 select count(*) from (select distinct tax_id, gene_id, symbol, chromosome, map_location, status  from gene_info_merged_clean) as x;

 select count(*) from
    (select distinct x.tax_id, x.gene_id, x.symbol, x.chromosome, x.map_location, y.status from gene_info_clean x, gene_status y where x.gene_id = y.gene_id) as x;



  WITH RECURSIVE successors AS (
   SELECT  go_id, go_node
     FROM go_relationships
     WHERE go_node = 'GO:0000012'
   UNION
   SELECT at.go_node, at.go_id
     FROM go_relationships at
     JOIN successors a
       ON (at.go_node = a.go_id)
 )
   SELECT * FROM successors;


   CREATE TABLE go_relationships (
   	id ingteger PRIMARY KEY, -- the edge id
   	go_id varchar(10), -- the starting GO node
   	go_node varchar(10), -- the end GO node
   );

   CREATE TABLE go_ontology (
   	   id varchar(10),
       name varchar(100),
       namespace varchar(100)
   );

 WITH RECURSIVE search_graph(id, link, depth) AS (
       SELECT g.go_id, g.go_node, 1
       FROM go_relationships g
     UNION ALL
       SELECT g.go_id, g.go_node, sg.depth + 1
       FROM go_relationships g, search_graph sg
       WHERE g.go_id = sg.link
)
SELECT * FROM search_graph;
