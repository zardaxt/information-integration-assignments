#!/usr/bin/env python3

"""
This script finds the successors of a given
node in the Gene Ontology.
http://www.vertabelo.com/blog/technical-articles/sql-recursive-queries
"""

import pprint
from transitive_closure_dag import get_edges_as_dict
from csv_tools import csv_as_list

edges = get_edges_as_dict()
edges = {k:v for k,v in edges.items() if k and v != ['']}

data = csv_as_list('terms.csv')
data = {id:[name, namespace] for id,name,namespace,_ in data}

def get_successors(L, node, dist=0):
    successors = edges.get(node, None)
    if not successors:
        return node

    for suc in successors:
        L.append((suc, dist))
        L.append(get_successors(L, suc, dist+1))

    return L

def dfs(start):
    """
    http://eddmann.com/posts/depth-first-search-and-breadth-first-search-in-python/
    """
    d = 0
    visited, stack = set(), [(start, d)]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.add(vertex)
            neighbors = set(edges.get(vertex[0], {}))
            neighbors = {(n, vertex[1]+1) for n in neighbors}
            stack.extend(neighbors - visited)
    return visited

def print_successors(node):
    tree = dfs(node)
    dag = [(node[0], node[1], data[node[0]][0]) for node in tree]
    pprint.pprint(sorted(dag, key=lambda x: x[1]))

if __name__ == '__main__':
    print_successors('GO:0046483')
