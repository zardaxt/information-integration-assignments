INSERT INTO gene_info_merged(
  tax_id,
  gene_id,
  symbol,
  chromosome,
  map_location,
  status,
  protein_accession,
  protein_accession_version,
  start_position_on_the_genomic_accession,
  end_position_on_the_genomic_accession)
  (SELECT
    gi.tax_id,
    gi.gene_id,
    gi.symbol,
    gi.chromosome,
    gi.map_location,
    grefseq.status,
    grefseq.protein_accession,
    grefseq.protein_accession_version,
    grefseq.start_position_on_the_genomic_accession,
    grefseq.end_position_on_the_genomic_accession
    FROM gene_info gi INNER JOIN gene2refseq grefseq
    ON (gi.gene_id = grefseq.gene_id)
);
