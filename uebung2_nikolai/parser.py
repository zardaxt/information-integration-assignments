#!/usr/bin/env python3

import csv

def merge():
    """
    merge_gene2refseq and_geneinfo
    Only keep human rows.
    """
    outfile = open('merged_gene_info.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'symbol', 'chromosome', 'map_location', 'status',
     'protein_accession_version', 'protein_gi', 'start_position_on_the_genomic_accession', 'end_position_on_the_genomic_accession'])

    info_file = open('gene_info_processed.csv', newline='')
    reader_info = csv.reader(info_file, delimiter='\t')
    header = next(reader_info)

    gene_info_data = dict()
    keys = set()

    h = nh = 0
    for row in reader_info:
        if row[0].strip() == '9606' and int(row[0]) == 9606:
            h+=1
            key = (row[0], row[1])
            gene_info_data[key] = [row[2], row[3], row[4]]
            keys.add(key)
        else:
            nh+=1

    print(h, nh)

    refseq_file = open('gene2refseq_processed.csv', newline='')
    reader_refseq = csv.reader(refseq_file, delimiter='\t')
    header_refseq = next(reader_refseq)

    for row in reader_refseq:
        out_tuple = []
        refseq_key = (row[0], row[1])
        out_tuple.extend(refseq_key)

        if refseq_key in gene_info_data:
            out_tuple.extend(gene_info_data[refseq_key])
            out_tuple.extend(row[2:])
            if refseq_key in keys:
                keys.remove(refseq_key)
            writer.writerow(out_tuple)
        # key cannot be found in gene_info but tax_id is still human
        elif row[0].strip() == '9606' and int(row[0]) == 9606:
            out_tuple.extend(3*[''])
            out_tuple.extend(row[2:])
            writer.writerow(out_tuple)

    print('Keys in gene_info but not in gene2refseq: {}'.format(len(keys)))

    for key in keys:
        t = gene_info_data[key]
        writer.writerow([key[0], key[1]] + gene_info_data[key] + (5 * ['']))

    outfile.close()


def get_duplicate_rows():
    """
    Walks through the merged_gene_info file and
    prints all duplicate rows with the same gene_id.
    """
    with open('merged_gene_info.csv', newline='') as f:
        reader = csv.reader(f, delimiter='\t')
        header = next(reader)

        gene_ids = dict()
        for row in reader:
            gid = row[1]
            if gid not in gene_ids:
                gene_ids[gid] = 1
            else:
                gene_ids[gid] += 1


    for i in range(10):
        print(i, len([k for k,v in gene_ids.items() if v == i]))

def findDR():
    """
    Detects functional dependiencies.
    A -> (B ... X)
    """
    duplicate_gene_ids = get_duplicate_rows()

    d = dict()

    with open('merged_gene_info.csv', newline='') as f:
        reader = csv.reader(f, delimiter='\t')
        header = next(reader)
        n = 0
        for row in reader:
            gid = row[1]
            if gid == '105379852':
                if 246998 == int(row[7]) and 356319 == int(row[8]):
                    n += 1
    print(n)



def get_null_value(c):
    if c.strip() == '-':
        return ''
    else:
        return c

def process_gene2refseq():
    outfile = open('gene2refseq_processed.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'status',
     'protein_accession_version', 'protein_gi', 'start', 'end'])

    with open('gene2refseq', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id = row[0]
            gene_id = row[1]
            status =  get_null_value(row[2])
            pa_version = row[5]
            pa_gi = row[6]
            start = get_null_value(row[9])
            end = get_null_value(row[10])
            writer.writerow([tax_id, gene_id, status, pa_version, pa_gi, start, end])
    outfile.close()


def count_unique_gene_ids(fname):
    s = set()

    with open(fname, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for i, row in enumerate(reader):
            s.add(row['gene_id'])

    return s


def count_all_unique_genes():
    all_gene_ids = set()
    for n in ('gene_info_processed.csv',):
        z = count_unique_gene_ids(n)
        print(len(z))
        all_gene_ids.union(z)
    print(len(all_gene_ids))


def get_intersection_count():
    first = count_unique_gene_ids('gene2refseq_processed.csv')
    second = count_unique_gene_ids('gene_info_processed.csv')

    print(len(first), len(second))
    print(len(first.intersection(second)))


def remove_csv_column():
    outfile = open('gene_info_processed_without_synonym.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'symbol', 'chromosome', 'map_location'])

    with open('gene_info_processed.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id, gene_id, symbol, chromosome, map_location = row[0], row[1], row[2], row[4], row[5]

            writer.writerow([tax_id, gene_id, get_null_value(symbol), get_null_value(chromosome), get_null_value(map_location)])
    outfile.close()


def process_gene_info():
    outfile = open('gene_info_processed.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'symbol', 'synonyms', 'chromosome', 'map_location'])

    with open('gene_info', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id, gene_id, symbol, synonyms, chromosome, map_location = row[0], row[1], row[2], row[4], row[6], row[7]
            writer.writerow([tax_id, gene_id, get_null_value(symbol),
             get_null_value(synonyms), get_null_value(chromosome), get_null_value(map_location)])
    outfile.close()


def process_gene2go():
    outfile = open('gene2go_processed.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'go_id', 'evidence'])

    with open('gene2go', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id, gene_id, go_id, evidence = row[0], row[1], row[2], row[3]
            writer.writerow([tax_id, gene_id, get_null_value(go_id), get_null_value(evidence)])
    outfile.close()

def check_data_types(fname='gene_info'):
    with open(fname, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        maxlength = 0
        n = 0
        for i, row in enumerate(reader):
            col = row[6]
            # print(col)
            if '|' in col:
                n += 1

            if len(col) > maxlength:
                maxlength = len(col)
                print(maxlength)
                print(col)
                print()

        print(n, i)

def store_in_new_table():
    with open('gene_info_processed.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        data = []
        for i, row in enumerate(reader):
            col = row[3]
            if col:
                if '|' in col:
                    items = set(map(str.strip, col.split('|')))
                else:
                    items = {col.strip()}

                gene_id = row[1].strip()
                for s in items:
                    if s:
                        data.append((gene_id, s))

        print(len(data))
        write_new_table(data, 'synonyms_gene_info.csv', ('gene_id', 'synonym'))


def count_distinct_chromosomes():
    with open('gene_info_processed.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        unique = set()
        for i, row in enumerate(reader):
            col = row[4]
            if col:
                if '|' in col:
                    items = set(map(str.strip, col.split('|')))
                else:
                    items = {col.strip()}
                print(items)
                for i in items:
                    unique.add(i)
        n = len(unique)
        if n < 1000:
            print(n, unique)
        print(n)


def write_new_table(data, fname, header):
    with open(fname, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)

        writer.writerow(header)

        for t in data:
            writer.writerow(t)

def test():
    print('hello')

if __name__ == '__main__':
    import sys
    if len(sys.argv) != 2:
        exit('Usage: {} function'.format(sys.argv[0]))
    else:
        globals()[sys.argv[1]]()
