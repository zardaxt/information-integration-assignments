Approach

First we write a Python script that processes the CSV files
gene2refseq and gene_info in order to clean the data structure.


1. NORMALIZE SYNONSYM COLUMN 

We are going to normalize the column gene_info.synonyms:

1. In First Normal Form, any row must not have a column in which
 more than one value is saved, like separated with commas.
Rather than that, we must separate such data into multiple rows.

2. As per the Second Normal Form there must not be any partial
dependency of any column on primary key.

=> synonyms will be separated into a new table with the schema 
(gene_id, synonym). Then all synonyms a specific
gene can be obtained with the query:
select * from gene_info_synonyms where gene_id = Z.
    
    
    
2. CREATE TABLE SCHEMA

We create CREATE TABLE statements by obtaining the attribute names 
from the README file. Then we detect the datatype by either the description
of the single attributes (as stated in the README file) or by inspecting
probes of each CSV files for each column. Similarilty we find out the maximum
length of the data in the CSV file. The script to reveal the data strucutre is
shown below:

    #!/usr/bin/env python3

    import csv

    def check_data_types(fname='gene_info.gz'):
        with open(fname, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            header = next(reader)
            maxlength = 0
            for i, row in enumerate(reader):
                col = row[2]
                if i % 100 == 0: #  only show some rows
                    # print(col)
                    if len(col) > maxlength:
                        maxlength = len(col)
                        print(maxlength)
                        
                        
3. SCHEMA INFORMATION

gene_id is the primary key for gene_info
there are 7,328,450 unique gene_id's in this schema.


gene_info has two columns with | seperated data. Normalize these attributes by 
creating own table. Affected columns: chromosome, map_location



