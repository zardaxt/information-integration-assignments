1. How many genes does your gene table have?

  Select count(distinct gene_id)
  from gene_info_merged;

  => 13754361

2. How many relationships between genes and a GO term are there?

	Select count(*)
  from gene_info_merged gim, gene2go g2g
  where gim.gene_id = g2g.gene_id;

	=> 13247063

3. How many distinct GO terms are annotated to at least one gene?

  What is a go term?

  SELECT COUNT(DISTINCT go_id) FROM gene2go WHERE gene_id IS NOT '';

4. How many gene names are present (symbol or synonym)?

  select count(distinct symbol)
  from gene_info_merged;

	=> 10776116

  select count(distinct synonym)
  from gene_synonyms;

  => 2425799

  => 10776116 + 2425799 = 13201915


5. How many synonyms are assigned to more than one gene?

    SELECT count(synonym)
    FROM gene_synonyms gs1, gene_synonyms gs2
    GROUP BY synonym
    HAVING count(gene_id) > 1;
