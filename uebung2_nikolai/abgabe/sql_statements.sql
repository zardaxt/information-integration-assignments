CREATE TABLE gene2go(
   -- the unique identifier provided by NCBI
   -- Taxonomy for the species or strain/isolate
   tax_id integer,
   -- the unique identifier for a gene
   gene_id integer,
   -- the GO ID, formatted as GO:0000000
   go_id varchar(10),
   -- the evidence code in the gene_association file
   evidence varchar(4)
);

CREATE TABLE gene_info (
   -- the unique identifier provided by NCBI Taxonomy
   tax_id integer,
   -- the unique identifier for a gene
   gene_id integer,
   -- the default symbol for the gene
   symbol varchar(90),
   --  the chromosome on which this gene is placed.
   -- for mitochondrial genomes, the value 'MT' is used.
   chromosome varchar(300),
   -- the map location for this gene
   map_location varchar(300)
);

CREATE TABLE gene_synonyms (
    gene_id integer, -- the unique identifier for a gene
    synonym varchar(500)
);


CREATE TYPE status_enum AS ENUM ('INFERRED', 'MODEL', 'NA', 'PREDICTED',
        'PROVISIONAL', 'REVIEWED', 'SUPPRESSED', 'VALIDATED', 'PIPELINE');

CREATE TABLE gene2refseq (
    -- the unique identifier provided by NCBI Taxonomy
    tax_id integer,
    -- the unique identifier for a gene
    gene_id integer,
    -- status of the RefSeq
    status status_enum,
    -- will be null (-) for RNA-coding genes
    protein_accession_version varchar(15),
    -- the gi for a protein accession, '-' if not applicable
    protein_gi integer,
    -- position of the gene feature on the genomic accession,
    start_position_on_the_genomic_accession varchar(15),
    -- position of the gene feature on the genomic accession, '-' if not applicable
    end_position_on_the_genomic_accession varchar(15)
);


-- this is the merged table of gene2refseq and gene_info
CREATE TABLE gene_info_merged (
   -- the unique identifier provided by NCBI Taxonomy
   tax_id integer,
   -- the unique identifier for a gene
   gene_id integer,
   -- the default symbol for the gene
   symbol varchar(90),
   --  the chromosome on which this gene is placed.
   -- for mitochondrial genomes, the value 'MT' is used.
   chromosome varchar(300),
   -- the map location for this gene
   map_location varchar(300),
   -- status of the RefSeq
   status status_enum,
   -- will be null (-) for RNA-coding genes
   protein_accession_version varchar(15),
   -- the gi for a protein accession, '-' if not applicable
   protein_gi integer,
   -- position of the gene feature on the genomic accession,
   start_position_on_the_genomic_accession integer,
   -- position of the gene feature on the genomic accession, '-' if not applicable
   end_position_on_the_genomic_accession integer
);
