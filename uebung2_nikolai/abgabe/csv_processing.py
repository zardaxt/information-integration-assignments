#!/usr/bin/env python3

import csv

def merge_gene2refseq_and_geneinfo():
    #outfile = open('merged_gene_info.csv', 'w', newline='')
    #writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)

    refseq_file = open('gene2refseq_processed.csv', newline='')
    reader_refseq = csv.reader(refseq_file, delimiter='\t')
    data = [row for row in reader_refseq]

    info_file = open('gene_info_processed.csv', newline='')
    reader_info = csv.reader(info_file, delimiter='\t')
    data2 = [row for row in reader_info]


def get_null_value(c):
    if c.strip() == '-':
        return ''
    else:
        return c

def process_gene2refseq():
    outfile = open('gene2refseq_processed.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'status',
     'protein_accession', 'protein_accession_version', 'start', 'end'])

    with open('gene2refseq', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id = row[0]
            gene_id = row[1]
            status =  get_null_value(row[2])
            pa = row[5]
            start = get_null_value(row[9])
            end = get_null_value(row[10])
            try:
                pa, pa_version = map(get_null_value, pa.split('.'))
            except:
                protein_accession, protein_accession_version = ('', '')
            writer.writerow([tax_id, gene_id, status, pa, pa_version, start, end])
    outfile.close()


def count_unique_gene_ids(fname):
    s = set()

    with open(fname, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for i, row in enumerate(reader):
            s.add(row['gene_id'])

    return s


def count_all_unique_genes():
    all_gene_ids = set()
    for n in ('gene_info_processed.csv',):
        z = count_unique_gene_ids(n)
        print(len(z))
        all_gene_ids.union(z)
    print(len(all_gene_ids))


def get_intersection_count():
    first = count_unique_gene_ids('gene2refseq_processed.csv')
    second = count_unique_gene_ids('gene_info_processed.csv')

    print(len(first), len(second))
    print(len(first.intersection(second)))


def remove_csv_column():
    outfile = open('gene_info_processed_without_synonym.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'symbol', 'chromosome', 'map_location'])

    with open('gene_info_processed.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id, gene_id, symbol, chromosome, map_location = row[0], row[1], row[2], row[4], row[5]

            writer.writerow([tax_id, gene_id, get_null_value(symbol), get_null_value(chromosome), get_null_value(map_location)])
    outfile.close()


def process_gene_info():
    outfile = open('gene_info_processed.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'symbol', 'synonyms', 'chromosome', 'map_location'])

    with open('gene_info', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id, gene_id, symbol, synonyms, chromosome, map_location = row[0], row[1], row[2], row[4], row[6], row[7]
            writer.writerow([tax_id, gene_id, get_null_value(symbol),
             get_null_value(synonyms), get_null_value(chromosome), get_null_value(map_location)])
    outfile.close()


def process_gene2go():
    outfile = open('gene2go_processed.csv', 'w', newline='')
    writer = csv.writer(outfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['tax_id', 'gene_id', 'go_id', 'evidence'])

    with open('gene2go', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for i, row in enumerate(reader):
            tax_id, gene_id, go_id, evidence = row[0], row[1], row[2], row[3]
            writer.writerow([tax_id, gene_id, get_null_value(go_id), get_null_value(evidence)])
    outfile.close()

def check_data_types(fname='gene_info'):
    with open(fname, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        maxlength = 0
        n = 0
        for i, row in enumerate(reader):
            col = row[6]
            # print(col)
            if '|' in col:
                n += 1

            if len(col) > maxlength:
                maxlength = len(col)
                print(maxlength)
                print(col)
                print()

        print(n, i)

def store_in_new_table():
    with open('gene_info_processed.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        data = []
        for i, row in enumerate(reader):
            col = row[3]
            if col:
                if '|' in col:
                    items = set(map(str.strip, col.split('|')))
                else:
                    items = {col.strip()}

                gene_id = row[1].strip()
                for s in items:
                    if s:
                        data.append((gene_id, s))

        print(len(data))
        write_new_table(data, 'synonyms_gene_info.csv', ('gene_id', 'synonym'))


def count_distinct_chromosomes():
    with open('gene_info_processed.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        unique = set()
        for i, row in enumerate(reader):
            col = row[4]
            if col:
                if '|' in col:
                    items = set(map(str.strip, col.split('|')))
                else:
                    items = {col.strip()}
                print(items)
                for i in items:
                    unique.add(i)
        n = len(unique)
        if n < 1000:
            print(n, unique)
        print(n)


def write_new_table(data, fname, header):
    with open(fname, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)

        writer.writerow(header)

        for t in data:
            writer.writerow(t)

if __name__ == '__main__':
    count_distinct_chromosomes()
