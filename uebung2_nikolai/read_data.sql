\copy gene2go FROM 'gene2go_processed.csv' WITH (FORMAT CSV, delimiter E'\t', HEADER);

\copy gene_info (tax_id, gene_id, symbol, chromosome, map_location) FROM 'gene_info_processed_without_synonym.csv' WITH (FORMAT CSV, delimiter E'\t', HEADER);

\copy gene_synonyms FROM 'synonyms_gene_info.csv' WITH (FORMAT CSV, delimiter E'\t', HEADER);

\copy gene2refseq FROM 'gene2refseq_processed.csv' WITH (NULL '-', FORMAT CSV, delimiter E'\t', HEADER);

\copy gene_info_merged FROM 'merged_gene_info.csv' WITH (NULL '', FORMAT CSV, delimiter E'\t', HEADER);
