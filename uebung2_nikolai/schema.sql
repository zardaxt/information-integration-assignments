
CREATE TABLE gene2go(
   tax_id integer, --  the unique identifier provided by NCBI Taxonomy for the species or strain/isolate
   gene_id integer, -- the unique identifier for a gene
   go_id varchar(10), -- the GO ID, formatted as GO:0000000
   evidence varchar(4) -- the evidence code in the gene_association file
);

copy gene2go from '/home/nikolai/Master/Informationsintegration/uebung2_nikolai/gene2go_processed.csv' with delimiter E'\t';

----------------------------------------------------------------------------------

CREATE TYPE status_enum AS ENUM ('INFERRED', 'MODEL', 'NA', 'PREDICTED', 'PROVISIONAL', 'REVIEWED', 'SUPPRESSED', 'VALIDATED', 'PIPELINE');

CREATE TABLE gene_info_merged (
    tax_id integer, -- the unique identifier provided by NCBI Taxonomy
    gene_id integer, -- the unique identifier for a gene
    symbol varchar(90), -- the default symbol for the gene

    -- this column is | separated, think about spacing out in anoter table
    chromosome varchar(166), --  the chromosome on which this gene is placed. for mitochondrial genomes, the value 'MT' is used.

    -- this column is | separated, think about spacing out in anoter table
    map_location varchar(300) -- the map location for this gene
    
    status status_enum, -- status of the RefSeq
    protein_accession_version varchar(30), -- will be null (-) for RNA-coding genes
    protein_gi bigint, -- the gi for a protein accession, '-' if not applicable
    start_position_on_the_genomic_accession bigint, -- position of the gene feature on the genomic accession,
    end_position_on_the_genomic_accession bigint, -- position of the gene feature on the genomic accession, '-' if not applicable
);

copy gene_info_merged from '/home/nikolai/Master/Informationsintegration/uebung2_nikolai/XXX.csv' with delimiter E'\t';


