# Format: tax_id GeneID GO_ID Evidence Qualifier GO_term PubMed Category (tab is used as a separator, pound sign - start of a comment)

CREATE TABLE gene2go(
   tax_id integer,
   gene_id integer,
   go_id varchar(11),
   evidence varchar(4),
   qualifier varchar(50),
   go_term varchar(200),
   pub_med text,
   category varchar(50)
);

CREATE TABLE gene2go(
   -- the unique identifier provided by NCBI
   -- Taxonomy for the species or strain/isolate
   tax_id integer,
   -- the unique identifier for a gene
   gene_id integer,
   -- the GO ID, formatted as GO:0000000
   go_id varchar(10),
   -- the evidence code in the gene_association file
   evidence varchar(4)
);

copy gene2go from '/home/nikolai/Master/Informationsintegration/uebung2_nikolai/gene2go_processed.csv' with delimiter E'\t';


###########################################################################


CREATE TABLE gene_info (
   tax_id integer, -- the unique identifier provided by NCBI Taxonomy
   gene_id integer, -- the unique identifier for a gene
   symbol varchar(50), -- the default symbol for the gene
   locus_tag varchar(30), -- the LocusTag value
   dbXrefs 150, -- bar-delimited set of identifiers in other databases for this gene.
   chromosome varchar(50), -- the chromosome on which this gene is placed.
   map_location varchar(30), -- the map location for this gene
   description varchar(400), -- a descriptive name for this gene
   type_of_gene varchar(20), -- the type assigned to the gene
   symbol_from_nomenclature_authority varchar(50), -- when not '-', indicates that this symbol is from a a nomenclature authority
   full_name_from_nomenclature_authority text, -- when not '-', indicates that this full name is from a a nomenclature authority
   nomenclature_status varchar(20), -- when not '-', indicates the status of the name from the  nomenclature authority
   other_designations text, -- pipe-delimited set of some alternate descriptions that have been assigned to a GeneID
   modification_date date, -- the last date a gene record was updated, in YYYYMMDD format
);

CREATE TABLE gene_info (
   -- the unique identifier provided by NCBI Taxonomy
   tax_id integer,
   -- the unique identifier for a gene
   gene_id integer,
   -- the default symbol for the gene
   symbol varchar(90),
   --  the chromosome on which this gene is placed.
   -- for mitochondrial genomes, the value 'MT' is used.
   chromosome varchar(166),
   -- the map location for this gene
   map_location varchar(300)
);

copy gene_info (tax_id, gene_id, symbol, chromosome, map_location)
from '/home/nikolai/Master/Informationsintegration/uebung2_nikolai/gene_info_processed_without_synonym.csv'
with (FORMAT CSV, delimiter E'\t', header);

select count(distinct gene_id) from gene_info;


###########################################################################


CREATE TYPE status_enum AS ENUM ('INFERRED', 'MODEL', 'NA', 'PREDICTED', 'PROVISIONAL', 'REVIEWED', 'SUPPRESSED', 'VALIDATED', 'PIPELINE');

CREATE TABLE gene2refseq (
    tax_id integer, -- the unique identifier provided by NCBI Taxonomy
    gene_id integer, -- the unique identifier for a gene
    status status_enum, -- status of the RefSeq
    RNA_nucleotide_accession_version varchar(50), -- may be null (-) for some genomes
    RNA_nucleotide_gi varchar(15), -- the gi for an RNA nucleotide accession, '-' if not applicable
    protein_accession_version varchar(30), -- will be null (-) for RNA-coding genes
    protein_gi varchar(15), -- the gi for a protein accession, '-' if not applicable
    genomic_nucleotide_accession_version varchar(30), -- may be null (-) if a RefSeq was provided after the genomic accession was submitted
    genomic_nucleotide_gi varchar(15), -- the gi for a genomic nucleotide accession, '-' if not applicable
    start_position_on_the_genomic_accession varchar(15), -- position of the gene feature on the genomic accession,
    end_position_on_the_genomic_accession varchar(15), -- position of the gene feature on the genomic accession, '-' if not applicable
    orientation varchar(10), -- orientation of the gene feature on the genomic accession, '?' if not applicable
    assembly varchar(100), -- the name of the assembly '-' if not applicable
    mature_peptide_accession_version varchar(50), -- will be null (-) if absent
    mature_peptide_gi varchar(15), -- the gi for a mature peptide accession, '-' if not applicable
    symbol varchar(50) -- the default symbol for the gene
);


CREATE TYPE status_enum AS ENUM ('INFERRED', 'MODEL', 'NA', 'PREDICTED', 'PROVISIONAL', 'REVIEWED', 'SUPPRESSED', 'VALIDATED', 'PIPELINE');

CREATE TABLE gene2refseq (
    -- the unique identifier provided by NCBI Taxonomy
    tax_id integer,
    -- the unique identifier for a gene
    gene_id integer,
    -- status of the RefSeq
    status status_enum,
    -- will be null (-) for RNA-coding genes
    protein_accession varchar(30),
    -- the gi for a protein accession, '-' if not applicable
    protein_accession_version varchar(10),
    -- position of the gene feature on the genomic accession,
    start_position_on_the_genomic_accession varchar(15),
    -- position of the gene feature on the genomic accession, '-' if not applicable
    end_position_on_the_genomic_accession varchar(15)
);

copy gene2refseq
FROM '/home/nikolai/Master/Informationsintegration/uebung2_nikolai/gene2refseq_processed.csv'
with (FORMAT CSV, delimiter E'\t', HEADER);

###########################################################################

CREATE TABLE gene_synonyms (
    gene_id integer, -- the unique identifier for a gene
    synonym varchar(500)
);

copy gene_synonyms
FROM '/home/nikolai/Master/Informationsintegration/uebung2_nikolai/synonyms_gene_info.csv'
with (FORMAT CSV, delimiter E'\t', HEADER);

