## Informations Integration Assignments


All big files (like gene2refseq) should have a file suffix `.dat`. They won't be uploaded. 
So please name all huge files with a file ending `.dat`, otherwise a lot of data garbage will be 
uploaded.

Also, all `.csv` files will be not uploaded to the repository.
