#!/usr/bin/env python3

import csv

def parse_gene_ontology(fname='go-basic.obo'):
    """
    Each new term starts with the delimiter [Term].

    We are interested in the
    - id
    - name
    - namespace
    And n is_a items.
    """
    with open(fname, 'rt') as f:
        terms = []
        term = dict()
        for i, line in enumerate(f):
            line = line.strip()

            if '[Term]' in line:
                if term:
                    if 'is_a' in term:
                        term['is_a'] = '|'.join(term['is_a'])
                    if not ('is_obsolete' in term and term['is_obsolete']):
                        terms.append(term)
                    term = dict()

            if line.startswith('id: '):
                term['id'] = line.lstrip('id: ')

            if line.startswith('name: '):
                term['name'] = line.lstrip('name: ')

            if line.startswith('namespace: '):
                term['namespace'] = line.lstrip('namespace: ')

            if line.startswith('is_obsolete: '):
                if line == 'is_obsolete: true':
                    term['is_obsolete'] = True

            if line.startswith('is_a: '):
                if 'is_a' not in term:
                    term['is_a'] = []
                isa = line.lstrip('is_a: ')
                isa = isa.split(' ! ')[0]
                term['is_a'].append(isa)

        with open('terms.csv', 'wt') as outfile:
            fieldnames = ('id', 'name', 'namespace', 'is_a')

            writer = csv.DictWriter(outfile, delimiter='\t', fieldnames=fieldnames)
            writer.writeheader()

            for term in terms:
                writer.writerow(term)

def create_DAG_csv_file():
    """
    This function creates a CSV file with all
    realationships between the GO terms.

    It outputs a CSV file with two columns:
    - go_id
    - go_node
    which are two vertices that form and edge.
    """
    with open('terms.csv', 'rt') as f:
        reader = csv.reader(f, delimiter='\t')

        ff = open('dag.csv', 'wt')
        writer = csv.writer(ff, delimiter='\t')
        writer.writerow(('go_id', 'go_node'))

        for row in reader:
            print(row)
            realationships = row[3].split('|')
            print(realationships)
            for rel in realationships:
                writer.writerow(row[0], rel)

        ff.close()

if __name__ == '__main__':
    create_DAG_csv_file()
