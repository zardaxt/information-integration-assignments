#!/usr/bin/env python3

# https://www.wikidata.org/wiki/Q26876

import requests
import re
import json
import data
import sys

def clean_name(name):
    name = name.strip()
    name = name.replace('\n', '')
    name = ' '.join([part[0].upper() + part[1:] for part in name.split(' ') if part.strip()])
    return name

sugg = re.compile(r'<suggestion data="(?P<suggestion>[\w ]*?)"/>')
debug = False

def main():
    if len(sys.argv) != 3:
        exit('Usage: {} inputfile outputfile'.format(sys.argv[0]))

    input_file, output_file = sys.argv[1:3]
    names = [clean_name(name) for name in open(input_file, 'r', encoding='utf8').readlines()]
    total = len(names)
    found = 0
    data = []

    for name in names:
        country_code = find_cc_long(name)

        if country_code:
            found += 1

        data.append((name, country_code))
        print(name, ' - ', country_code)

    print('Found {} from total {} names'.format(found, total))
    write_tsv_file(data, output_file)


def find_cc_short(name):
    # basic algorithm:
    # first check english wiki
    # if no result spell check name with bing
    # check english wiki with corrected name
    country_code = None
    lan = 'en'
    res = wiki_search(name, language=lan)
    if not res:
        real_name = bing_suggest2(name)

        res = wiki_search(name, language=lan)

    if res:
        country_code = get_country_code(res, lan=lan)

    return country_code

def find_cc_long(name):
    # basic algorithm:
    # first check english wiki
    # if no result check german wiki
    # if no result spell check name with bing
    # check english wiki with corrected name
    # check german wiki with corrected name
    country_code = None
    lan = 'en'
    res = wiki_search(name, language=lan)
    if not res:
        lan = 'de'
        res = wiki_search(name, language=lan)

        if not res:
            real_name = bing_suggest2(name)

            if real_name:
                real_name = clean_name(real_name)
                lan = 'en'
                res = wiki_search(real_name, language=lan)

                if not res:
                    lan = 'de'
                    res = wiki_search(real_name, language=lan)

    if res:
        country_code = get_country_code(res, lan=lan)

    return country_code

def write_tsv_file(data, output_file):
    """
    writes data to the .tsv file.
    """
    if not output_file.endswith('.tsv'):
        output_file += '.tsv'

    f = open(output_file, 'wt')
    for name, country in data:
        f.write('{}\t{}\n'.format(name, country))
    f.close()


def get_country_code(cleaned, lan='en'):
    if cleaned in data.us_states.values():
        return 'US'

    if 'U.S.' in cleaned:
        return 'US'

    if 'russia' in cleaned.lower():
        return 'RU'

    for c in ('England', 'Wales', 'Scotland'):
        if c in cleaned.lower():
            return 'GB'

    if lan == 'en':
        country_codes = data.country_codes
    elif lan == 'de':
        country_codes = data.country_codes_de

    try:
        return country_codes[cleaned]
    except KeyError as e:
        countries = list(country_codes.keys())
        min_distance = 1000
        cc = None
        for country in countries:
            dist = levenshtein(cleaned, country)
            if dist < min_distance:
                min_distance = dist
                cc = country_codes.get(country)
                if debug:
                    print(dist, cleaned, country, cc)
        return cc

def wiki_search(name, language='en'):
    if language == 'en':
        bp = re.compile(r'birth_place\s*?=\s*?(?P<bp>.*?)\\n')
        api = 'https://en.wikipedia.org/w/api.php?action=query&titles={}&prop=revisions&rvprop=content&format=json&redirects'
    elif language == 'de':
        # GEBURTSORT=[[Idar-Oberstein]], Rheinland-Pfalz, Deutschland\n
        bp = re.compile(r'GEBURTSORT\s*?=\s*?(?P<bp>.*?)\\n')
        api = 'https://de.wikipedia.org/w/api.php?action=query&titles={}&prop=revisions&rvprop=content&format=json&redirects'

    try:
        req = requests.get(api.format(name))
        js = json.loads(req.text)
    except Exception as e:
        if debug:
            print('Some error while fetching wiki info for {}'.format(name))
            print(str(e))
        return False

    try:
        birth_place = bp.search(req.text).group('bp')
        def allow(c):
            if c.isalnum() or c == ' ' or c == '.':
                return True
            return False

        cleaned = ''.join([c if allow(c) else '' for c in birth_place.split(',')[-1]])
        cleaned = cleaned.strip()
        return cleaned
    except:
        # maybe the name was misspelled
        return False

def google_suggest(name):
    """
    https://github.com/bkvirendra/didyoumean/
    Gets the google suggestion from a wrong name.
    """
    api = 'http://suggestqueries.google.com/complete/search?output=toolbar&hl=en&q={}'
    req = requests.get(api.format(name))
    try:
        real_name = sugg.search(req.text).group('suggestion')
    except AttributeError as e:
        print(req.text)
        print(name)
    return real_name

def bing_suggest(name):
    """
    get the bing suggest query to determine the real name
    """
    url = 'http://www.bing.com/search?q={}'
    req = requests.get(url.format(name))
    tree = html.fromstring(req.text)
    sel = CSSSelector('#sp_requery h2 a')

    try:
        res = sel(tree)
        return res[0].text_content()
    except:
        return False

def bing_suggest2(name):
    """
    get the bing suggest query to determine the real name

    not using lxml because we cannot install libraries on
    the running system. Parse manually.
    """
    url = 'http://www.bing.com/search?q={}'
    req = requests.get(url.format(name))
    html = req.text
    s = ''

    try:
        needle = 'id="sp_requery">'
        i = html.index(needle)
        while '</a>' not in s:
            s += html[i]
            i += 1

        match = re.search(r'<a.*?>(?P<corr>.*?)</a>', s).group('corr')
        result =  match.replace('<strong>', '').replace('</strong>', '')
        return result
    except Exception as e:
        ('*** {} - {} - {}\n'.format(name, s, str(e).strip()))
        return False

def levenshtein(a, b, debug=False):
    """
    Computes the Levenshtein edit distance between
    the string a and string b.

    requires:
        (len(a) > 0 && len(b) > 0)

    ensures:
        (retval >= 0 && retval <= max(a, b))
    """
    if not (len(a) > 0 and len(b) > 0):
        msg = 'Strings a and b cannot be empty.'
        raise ValueError(msg)

    D = []
    for i in range(len(a)):
        D.append([])
        for j in range(len(b)):
            D[i].append(0)

    for i in range(len(a)):
        D[i][0] = i

    for j in range(len(b)):
        D[0][j] = j

    for i in range(len(a)):
        for j in range(len(b)):
            cost = [1, 0][a[i] == b[j]]
            D[i][j] = min(D[i-1][j] + 1,
                D[i][j-1] + 1, D[i-1][j-1] + cost)

    if debug:
        pprint.pprint(D)

    return D[-1][-1]

if __name__ == '__main__':
    main()
